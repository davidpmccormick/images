import Ember from 'ember';

export default Ember.Service.extend({
  apiUrl: `https://api.wellcomecollection.org/catalogue/v1/works`,
  baseQueryParams: {
    includes: 'thumbnail,identifiers',
    pageSize: 100
  },
  totalPages: 100, // Only the first 10000 works available in the API
  resultsPerPage: 100,
  randomIntFromInterval(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
  },
  getRandomImage() {
    const randomPage = this.randomIntFromInterval(1, this.get('totalPages'));
    const randomResult = this.randomIntFromInterval(1, this.get('resultsPerPage'));

    return this.getImagesByParams({page: randomPage}).then(data => {
      return data.results[randomResult];
    });
  },
  getImages() {
    return Ember.$.ajax({
      url: `${this.apiUrl}`,
      data: this.get('baseQueryParams')
    });
  },
  getImageById(id) {
    return Ember.$.ajax({
      url: `${this.apiUrl}/${id}`,
      data: this.get('baseQueryParams')
    });
  },
  getImagesByParams(params) {
    return Ember.$.ajax({
      url: `${this.apiUrl}`,
      data: Object.assign({}, this.get('baseQueryParams'), params)
    });
  },
  getParams(url) {
    if (!url) return {};

    return (/^[?#]/.test(url) ? url.slice(1) : url)
      .split('&')
      .reduce((params, param) => {
        let [key, value] = param.split('=');
        params[key] = value
          ? decodeURIComponent(value.replace(/\+/g, ' '))
          : '';
        return params;
      }, {});
  }
});
