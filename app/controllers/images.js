import Ember from 'ember';

export default Ember.Controller.extend({
  query: '',
  page: 1,
  isSmallInput: false,
  images: Ember.inject.service(),
  nextPage: Ember.computed('model', function() {
    return this.get('images')
      .getParams(this.get('model.nextPage'))
      .page;
  }),
  prevPage: Ember.computed('model', function() {
    return this.get('images')
      .getParams(this.get('model.prevPage'))
      .page;
  }),
  goSmall() {
    this.set('randomImage', null);
    this.set('isSmallInput', true);
  },
  setRandom(value) {
    if (value) {
      this.set('isSmallInput', false);
      this.set('randomImage', true);
    } else {
      this.set('randomImage', false);
    }
  },
  actions: {
    updateSearch(query) {
      this.set('page', 1);
      this.set('query', query);
    }
  }
});
