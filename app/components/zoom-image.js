import Ember from 'ember';
import OpenSeadragon from 'openseadragon';

export default Ember.Component.extend({
  classNames: ['zoom-view'],
  async _setupViewer() {
    const imageUrl = this.get('model.thumbnail.url');
    const imageInfoUrl = `${imageUrl.match(
      /^https:\/\/iiif\.wellcomecollection\.org\/image\/(.+?\.[a-z]{3})/
    )[0]}/info.json`;
    const imageInfo = await Ember.$.get(imageInfoUrl);
    const viewer = OpenSeadragon({
      id: 'zoom',
      visibilityRatio: 1,
      showFullPageControl: false,
      zoomInButton: 'zoom-in',
      zoomOutButton: 'zoom-out',
      homeButton: 'home',
      showNavigator: true,
      controlsFadeDelay: 0,
      tileSources: [
        {
          '@context': 'http://iiif.io/api/image/2/context.json',
          '@id': imageInfo['@id'],
          height: imageInfo.height,
          width: imageInfo.width,
          profile: ['http://iiif.io/api/image/2/level2.json'],
          protocol: 'http://iiif.io/api/image',
          tiles: [
            {
              scaleFactors: [1, 2, 4, 8, 16, 32],
              width: 400
            }
          ]
        }
      ]
    });
    this.set('viewer', viewer);
  },
  didUpdateAttrs() {
    const viewer = this.get('viewer');

    if (!viewer) return;

    viewer.destroy();
    this.set('isZooming', false);
  },
  didReceiveAttrs() {
    this.getEmotion();
  },
  getEmotion() {
    Ember.$.ajax({
      url: `https://westus.api.cognitive.microsoft.com/emotion/v1.0/recognize?`,
      beforeSend: function(xhrObj) {
        xhrObj.setRequestHeader('Content-Type', 'application/json');
        xhrObj.setRequestHeader('Ocp-Apim-Subscription-Key', 'aba7ecd9eb1b45be9d072e15419306f3');
      },
      type: 'POST',
      data: `{
        'url': '${this.get('model.thumbnail.url')}'
      }`
    }).done((data) => {
      const actualImageWidth = this.$('img').width();
      const scaleFactor = actualImageWidth / 300;
      data.forEach(item => {
        Object.keys(item.faceRectangle).forEach(key => {
          item.faceRectangle[key] = item.faceRectangle[key] * scaleFactor;
        });
        Object.keys(item.scores).forEach(key => {
          item.scores[key] = Math.round(item.scores[key] * 100);
        });
      });

      this.set('emotions', data);
    });
  },
  actions: {
    doZoom() {
      this._setupViewer().then(() => {
        this.set('isZooming', true);
      });
    },
    exitZoom() {
      this.get('viewer').destroy();
      this.set('isZooming', false);
    }
  }
});
