import Ember from 'ember';

export default Ember.Component.extend({
  images: Ember.inject.service(),
  getRandomImage() {
    this.$().fadeTo(500, 0, () => {
      this.set('isActive', false);
      this.get('images').getRandomImage().then(img => {
        this.set('image', img);

        this.$('img').on('load', () => {
          if (this.isDestroying || this.isDestroyed) return;

          this.set('isActive', true);
          this.$().fadeTo(500, 1);
        });
      });
    });
  },
  didInsertElement() {
    this.getRandomImage();
    this._timer = setInterval(() => {
      this.getRandomImage();
    }, 10000);
  },
  willDestroyElement() {
    clearInterval(this._timer);
  },
  classNames: ['random-image'],
  classNameBindings: ['isActive']
});
