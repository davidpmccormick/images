import Ember from 'ember';

export default Ember.Component.extend({
  didInsertElement() {
    const initialValue = this.$().find('input')[0].value;

    this.set('isInputEntered', initialValue);
  },
  classNames: ['search-box'],
  classNameBindings: ['isSmallInput:is-small-input'],
  actions: {
    onKeyUpHandler(value) {
      this.set('isInputEntered', value.length);
    },
    onSubmitHandler() {
      event.preventDefault();

      const inputValue = event.target[0].value;

      this.attrs.onSubmitHandler(inputValue);
    },
    onClearInputHandler() {
      const input = this.$().find('input')[0];

      input.focus();
      input.value = '';
      this.set('isInputEntered', false);
    }
  }
});
