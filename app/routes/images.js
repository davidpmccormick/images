import Ember from 'ember';

export default Ember.Route.extend({
  images: Ember.inject.service(),
  queryParams: {
    query: {
      refreshModel: true
    },
    page: {
      refreshModel: true
    }
  },
  model(params) {
    const imagesController = this.controllerFor('images');

    if (!params.query) {
      imagesController.setRandom(true);
      this.transitionTo('images', {queryParams: {page: 1}});

      return false;
    } else {
      imagesController.setRandom(false);
      imagesController.goSmall();
      return this.get('images').getImagesByParams(params);
    }
  },
  actions: {
    loading(transition, originRoute) {
      const imagesController = this.controllerFor('images');

      imagesController.set('isLoading', true);
      transition.promise.finally(() => imagesController.set('isLoading', false));
    }
  }
});
