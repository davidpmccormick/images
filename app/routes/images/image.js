import Ember from 'ember';

export default Ember.Route.extend({
  images: Ember.inject.service(),
  model({ id }) {
    this.controllerFor('images').setRandom(false);
    this.controllerFor('images').goSmall();
    return this.get('images').getImageById(id);
  }
});
