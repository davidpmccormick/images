module.exports = {
  root: true,
  parserOptions: {
    ecmaVersion: 2017,
    sourceType: 'module'
  },
  extends: 'standard',
  env: {
    browser: true
  },
  rules: {
    'semi': ['error', 'always'],
    'space-before-function-paren': ['error', 'never']
  }
};
